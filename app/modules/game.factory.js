angular
    .module('game',[])
    .factory('GameFactory',function() {
        return function(scope) {
            var game = { // es5 para propósito de compatibilidade
                restart:function() {
                    scope.data.deck = [];
                    this.points.init();
                    this.card.remaining = +this.card.limit;
                    this.card.deck();
                    scope.data.congrats = false;
                    this.card.bluffs = [];
                },
                finish:function() {
                    var message = 'Pontuação baixa. Toma memoriol e volta.';
                    this.rounds.add(scope.data.name);
                    var points = this.points.sum;
                    if(points > 1800) {
                        message = 'Essa pontuação, só com cheat ou script. Zerada. ;D';
                        if(!game.test.running) game.rounds.punish();
                    } else if(points > 1700) {
                        message = 'Pontuação top, você está fazendo pontos demais... está sem serviço?';
                    } else if(points > 1500)  {
                        message = 'Pontuação alta, você lembra o que comeu ontem.';
                    } else if(points > 1300)  {
                        message = 'Pontuação média, preste mais atenção que melhora.';
                    }
                    scope.data.congrats = message;
                },
                points:{
                    init:function() {
                        this.sum = 0;
                        this.run();
                    },
                    add:function() {
                        this.sum += this.worth;
                        scope.data.sum = this.sum;
                    },
                    run:function() {
                        var me = this;
                        me.worth = 200;
                        clearInterval(me.timer);
                        me.timer = setInterval(function() {
                            me.worth--;
                            if(me.worth==1) clearInterval(me.timer);
                        },1000);
                    }
                },
                rounds:{
                    list:[],
                    add:function(user) {
                        this.list.unshift({
                            user:game.test.running?'Computador':user,
                            round:this.list.length,
                            points: game.points.sum
                        });
                        if(this.list.length>8) this.list.pop();
                        scope.data.ranking = [].slice.call(this.ranking());
                        setTimeout(function() {
                            scope.$apply();
                        },500);
                    },
                    punish:function() {
                        this.list[this.list.length-1].points = 0;
                        scope.data.ranking = [].slice.call(this.ranking());
                        setTimeout(function() {
                            scope.$apply();
                        },500);
                    },
                    ranking:function() {
                        return this.list.sort(function(a,b) {
                            return a.points != b.points? a.points < b.points ? 1 : -1 : 0;
                        }) || [];
                    }
                },
                card:{
                    limit:10,
                    bluffs:[],
                    cache:{},
                    suits:['heart','diamond','spades','club'],
                    cards:['k','q','j',10,9,8,7,6,5,4,3,2,'a'],
                    generate:function() {
                        var randomAddress = function(arr) {
                            return arr[Math.floor(Math.random()*arr.length)];
                        }
        
                        var card = {
                            suit: randomAddress(this.suits),
                            card: randomAddress(this.cards).toString(),
                            closed:true
                        };
                        card.label = card.card.toUpperCase(); // sem maiusculas no css
        
                        var cardString = `${card.suit}:${card.card}`;
        
                        if(this.cache[cardString]) return this.generate();
                        this.cache[cardString] = true;
        
                        return card;
                    },
                    position: function() { return Math.floor(Math.random()*this.limit*2) },
                    deck: function() {
                        var me = this;
                        this.cache = {};
                        var set = [];
                        var apply = function(card) {
                            var pos = me.position();
                            while(set[pos]) pos = me.position();
                            set[pos] = Object.assign({},card);
                        }
                        for(var i = 0;i<this.limit;i++) {
                            var card = this.generate();
                            apply(card);
                            apply(card);
                        }

                        scope.data.deck = [].slice.call(set);
                    },
                    flip:function(index) {
                        var card = scope.data.deck[index];
                        if(card.done || !card.closed) return;
                        var me = this;
                        var card = scope.data.deck[index];
                        card.closed = !card.closed;
                        me.bluffs.push(index);
                        if(me.bluffs.length>1) {
                            var first = scope.data.deck[me.bluffs[0]], last = scope.data.deck[me.bluffs[1]];
                            if(me.bluffs.length>2) {
                                me.closeBluffs();
                                me.bluffs = [me.bluffs[2]];
                            } else if(first.card == last.card && first.suit == last.suit) {
                                me.isMatch();
                                me.bluffs = [];
                            }
                        }
                    },
                    isMatch:function() {
                        scope.game.points.add();
                        for(var i=0;i<2;i++) {
                            let index = this.bluffs[i]
                            scope.data.deck[index].closed = false;
                            scope.data.deck[index].done = true;
                            scope.data.stringDeck = JSON.stringify(scope.data.deck,null,2);
                        }
                        this.remaining--;
                        if(!this.remaining) game.finish();
                    },
                    closeBluffs:function() {
                        for(var i=0;i<2;i++) {
                            let index = this.bluffs[i]
                            scope.data.deck[index].closed = true;
                        }
                    }
                },
        
                test:{
                    interval:100,
                    counter:0,
                    perform:function(velocity) {
                        var me = this;
                        this.running = true;
                        this.timer = setInterval(function() {
                            if(me.counter == 20) {
                                me.counter = 0;
                                scope.$digest();
                            }
                            game.test.autoplay();
                            me.counter++;
                        },velocity || this.interval)
                    },
                    end:function() {
                        this.running = false;
                        clearInterval(this.timer);
                        this.counter = 0;
                        game.restart();
                    },
                    megs:function(num) {
                        return `${Math.ceil((num/1024/1024*100))/100} MB`
                    },
                    autoplay:function() {
                        if(!this.autoplayDeck) {
                            game.restart();
                        }
                        var deck = scope.data.deck;
                        this.autoplayDeck = deck;
                        var cardsLeft = deck.filter(function(each) {return !each.done}).length;
                
                        if(!cardsLeft) {
                            var points = game.points.sum;
                            var round = game.rounds.list.length;
                            delete this.autoplayDeck;
                            game.test.autoplay();
                            console.log(`Pontos: ${points}
Rodada: ${round}
Ranking:`,game.rounds.ranking());
                            try {
                                console.log('usoTotal',game.test.megs(process.memoryUsage().heapUsed));
                            } catch(e) { // chrome only
                                console.log('usoTotal',game.test.megs(window.performance.memory.totalJSHeapSize));
                            }
                            return;
                        }
                
                        var pos = game.card.position();
                        while(deck[pos].done || this.autoplayLastPos == pos) pos = game.card.position();
                        game.card.flip(pos);
                        game.autoplayLastPos = pos;
                    }
                }
            }
            return game;
        } 
    })
;