angular
    .module('app',['game'])
    .controller('AppController',['$scope','GameFactory',function($scope,GameFactory) {

        $scope.data = {
            sum:0,
            getDeck:function() {
                return this.deck || [];
            },
            getRanking:function() {
                return this.ranking || [];
            },
            getName:function() {
                return $scope.game.test.running?'Computador':this.name;
            }
        };
        $scope.game = GameFactory($scope);

        $scope.events = {
            autoplayLabel:'Autoplay',
            autoplay:function() {
                $scope.game.autoplaying = !$scope.game.autoplaying;
                if($scope.game.autoplaying) {
                    this.autoplayLabel = 'Parar autoplay';
                    $scope.game.test.perform();
                } else {
                    this.autoplayLabel = 'Autoplay';
                    $scope.game.test.end();
                }
            },
            start:function () {
                $scope.game.restart();
                $scope.data.name = $scope.data.name.toUpperCase();
                $scope.data.readonly = true;
            }
        }
    }]);
