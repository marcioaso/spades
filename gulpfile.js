// UTIL
String.prototype.versionize = function() {
    var rand = Math.random().toString(36).substring(7);
    return this.replace('.','.'+rand+'.');
};

var dev = true;
var gulp = require('gulp');
var express = require('express');
var app = express();
var livereload = require('gulp-livereload');
var refresh = require('gulp-refresh');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');


function plumbError() {
    return plumber({
        errorHandler: function(err) {
            notify.onError({
            templateOptions: {
            date: new Date()
        },
            title: "Gulp error in " + err.plugin,
            message:  err.formatted
        })(err);
            this.emit('end');
        }
    })
}
gulp.task('serve',function() {
    app.use('/',express.static('app'));
    app.listen(9000);
    livereload.listen();

    gulp.watch([
        './app/**/*.js',
        './app/**/*.html',
        './app/**/*.css'
    ],function(e) {
        livereload.reload();
    });
});